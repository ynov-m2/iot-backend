import datetime
import enum

from uuid import uuid4
from sqlalchemy import Column, Integer, String, DateTime, Boolean, desc, Enum, ForeignKey, asc, func, Float
from sqlalchemy.orm import relationship, backref
from common.server.flaskutils import print_flush
from common.db.tools import to_dict, serialize, serialize_foreign, serialize_foreign_recursive
from common.db.base import Base
from common.utils.security import verify_password, hash_password

from config.settings import SETTINGS
from utils.permissions import Permissions


# class ScanSMState(enum.Enum):
#     SUPERMARKET_SLOW = "slow"
#     SUPERMARKET_STD = "standard"
#     SUPERMARKET_FAST = "fast"


# class ExAmple(Base):
#     __tablename__ = 'ex_ample'

#     uniqid = Column(String(36), primary_key=True)
#     id_kanban = Column(String(50), unique=False)
#     created_at = Column(DateTime, unique=False)
#     pos_x = Column(Integer)
#     pos_y = Column(String(50))
#     pos_z = Column(String(50))
#     seconds_since_last_scan = Column(Integer)
#     proceed_at = Column(DateTime, unique=False)
#     state = Column(Enum(ScanSMState), default=ScanSMState.SUPERMARKET_STD)

#     def __init__(self, name, x, y, z, state=ScanSMState.SUPERMARKET_STD, proceed_at=None, db=None):
#         self.uniqid = str(uuid4())
#         self.id_kanban = name
#         self.created_at = datetime.datetime.utcnow().isoformat()
#         self.pos_x = x
#         self.pos_y = y
#         self.pos_z = z
#         self.proceed_at = proceed_at
#         self.state = state


#     def __repr__(self):
#         return '<ScanSM %r (%s) %s>' % (self.id_kanban, self.uniqid, str(self.proceed_at))



class Account(Base):
    __tablename__ = 'account'

    uniqid = Column(String(36), primary_key=True)
    password = Column(String(89), nullable=True)
    email = Column(String(120), nullable=True)
    name = Column(String(50), unique=True)
    permission = Column(String(34))
    created_at = Column(DateTime, unique=False, default=datetime.datetime.utcnow)

    def __init__(self, name, permission=Permissions.GENERIC):
        self.uniqid = str(uuid4())
        self.name = name
        self.password = None
        self.email = None
        self.permission = permission
        self.created_at = datetime.datetime.utcnow()

    def set_password(self, password):
        self.password = hash_password(password)

    def check_password(self, password):
        return verify_password(self.password, password)

    def get_data(self):
        return {'uniqid': self.uniqid,
                'name': self.name,
                'permission': self.permission,
                'email': self.email}

    def __repr__(self):
        return '<Account %r (%s)>' % (self.name, self.uniqid)

class Device(Base):
    __tablename__ = 'device'

    uniqid = Column(String(36), primary_key=True)
    name = Column(String(36))
    posx = Column(Float)
    posy = Column(Float)
    created_at = Column(DateTime, unique=False, default=datetime.datetime.utcnow)

    def __init__(self, name, posx, posy):
        self.uniqid = str(uuid4())
        self.name = name
        self.posx = posx
        self.posy = posy
        self.created_at = datetime.datetime.utcnow()

class DeviceInfos(Base):
    __tablename__ = 'device_infos'

    uniqid = Column(String(36), primary_key=True)
    wind = Column(Float)
    humidity = Column(Float)
    temperature = Column(Float)

    device_id = Column(String(36), ForeignKey('device.uniqid', ondelete="CASCADE"))
    device = relationship('Device', backref=backref(
        'devices', lazy='dynamic'), foreign_keys=[device_id])

    created_at = Column(DateTime, unique=False, default=datetime.datetime.utcnow)

    def __init__(self, wind, humidity, temperature, device):
        self.uniqid = str(uuid4())
        self.wind = wind
        self.humidity = humidity
        self.temperature = temperature
        self.device = device
        self.created_at = datetime.datetime.utcnow()