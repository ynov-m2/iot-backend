import logging

from flask import request, jsonify, abort, Blueprint
from flask_restful import Resource, reqparse
from db.models import Device
from common.db.base import Database
from flask_restful_swagger import swagger


class DevicePosition(Resource):
    @swagger.operation(
        notes='Route to get all devices position',
        responseMessages=[
            {
              "code": 200,
              "message": "Everything went right. "
            }
        ]
    )
    def get(self):
        return_list = []
        with Database(auto_commit=True) as db:
            all_devices = db.query(Device).all()
            for device in all_devices:
                return_list.append({'position': {'lat': device.posx, 'lng': device.posy}})
            return jsonify(return_list)