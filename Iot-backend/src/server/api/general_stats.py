import logging
import datetime

from flask import request, jsonify, abort, Blueprint
from flask_restful import Resource, reqparse
from db.models import Device, DeviceInfos
from sqlalchemy import desc, func
from common.db.base import Database
from flask_restful_swagger import swagger


LOG = logging.getLogger(__name__)

class GeneralStats(Resource):
    @swagger.operation(
        notes='Route to get min/max values and average for every DeviceInfos if no device specified. Returns only General stats for a device if one is specified ',
        parameters=[
            {
              "name": "device_given",
              "description": "Device name",
              "required": False,
              "allowMultiple": False,
              "dataType": "string"
            }
        ],
        responseMessages=[
            {
              "code": 200,
              "message": "Everything went right"
            },
            {
              "code": 500,
              "message": "Something went wrong"
            }
        ]
    )
    def get(self, device_given):
        return_list = []
        twenty_minutes_ago = datetime.datetime.now() - datetime.timedelta(minutes=20)
        with Database(auto_commit=True) as db:
            device_query = db.query(Device)
            if not device_given == 'undefined':
                device_query = device_query.filter_by(name=device_given)
            all_devices = device_query.all()
            for device in all_devices:
                mean_infos = db.query(func.avg(DeviceInfos.wind), func.avg(DeviceInfos.humidity), func.avg(DeviceInfos.temperature))\
                                .filter_by(device=device)\
                                .filter(DeviceInfos.created_at >= twenty_minutes_ago)\
                                .first()
                max_infos = db.query(func.max(DeviceInfos.wind), func.max(DeviceInfos.humidity), func.max(DeviceInfos.temperature))\
                                .filter_by(device=device)\
                                .filter(DeviceInfos.created_at >= twenty_minutes_ago)\
                                .first()

                min_infos = db.query(func.min(DeviceInfos.wind), func.min(DeviceInfos.humidity), func.min(DeviceInfos.temperature))\
                                .filter_by(device=device)\
                                .filter(DeviceInfos.created_at >= twenty_minutes_ago)\
                                .first()

                return_list.append({'name': device.name,
                                    'wind': {'mean': mean_infos[0],
                                            'max': max_infos[0],
                                            'min': min_infos[0]},
                                    'humidity': {'mean': mean_infos[1],
                                            'max': max_infos[1],
                                            'min': min_infos[1]},
                                    'temperature': {'mean': mean_infos[2],
                                            'max': max_infos[2],
                                            'min': min_infos[2]}
                                    })
        return jsonify(return_list)