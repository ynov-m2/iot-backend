import logging
import datetime
import requests

from flask import request, jsonify, abort, Blueprint
from flask_restful import Resource, reqparse
from db.models import Device, DeviceInfos
from sqlalchemy import desc, func
from common.db.base import Database
from config.settings import SETTINGS
from flask_restful_swagger import swagger


LOG = logging.getLogger(__name__)

class HealthCheck(Resource):
    @swagger.operation(
        notes='Route to check if all device are up, or only for one device if device_given is specified',
        parameters=[
            {
              "name": "device_given",
              "description": "Device name",
              "required": False,
              "allowMultiple": False,
              "dataType": "string"
            }
        ],
        responseMessages=[
            {
              "code": 200,
              "message": "Everything went right"
            },
            {
              "code": 500,
              "message": "Something went wrong"
            }
        ]
    )
    def get(self, device_given):
        return_list = []
        with Database(auto_commit=True) as db:
            device_query = db.query(Device)
            if not device_given == 'undefined':
                device_query = device_query.filter_by(name=device_given)
            all_devices = device_query.all()
            for device in all_devices:
                device_here = requests.get(SETTINGS['API_URL'] + SETTINGS['DEVICE_UP'] + device.name).json()['device']
                return_list.append({'device': device.name, 'up': device_here, 'time': (datetime.datetime.utcnow() + datetime.timedelta(hours=2)).strftime('%B %d %Y - %H:%M:%S')})
            return jsonify(return_list)
