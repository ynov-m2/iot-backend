import logging

from flask import request, jsonify, abort, Blueprint
from flask_restful import Resource, reqparse
from db.models import Device, DeviceInfos
from sqlalchemy import desc
from common.db.base import Database
from flask_restful_swagger import swagger


LOG = logging.getLogger(__name__)

PARSER = reqparse.RequestParser()
PARSER.add_argument('user')
PARSER.add_argument('password')
PARSER.add_argument('mail')

class Stats(Resource):
    @swagger.operation(
        notes='Route that returns last 20 stats for every device if no parameter is specified or fone the specified device if one is given',
        parameters=[
            {
              "name": "device_given",
              "description": "Device name",
              "required": False,
              "allowMultiple": False,
              "dataType": "string"
            }
        ],
        responseMessages=[
            {
              "code": 200,
              "message": "Everything went right"
            },
            {
              "code": 500,
              "message": "something went wrong"
            }
        ]
    )
    def get(self, device_given):
        return_list = {'wind': [], 'humidity': [], 'temp': []}
        with Database(auto_commit=True) as db:
            device_query = db.query(Device)
            if not device_given == 'undefined':
                device_query = device_query.filter_by(name=device_given)
            all_devices = device_query.all()

            for device in all_devices:
                wind_dict = []
                humidity_dict = []
                temp_dict = []
                all_infos = db.query(DeviceInfos)\
                            .filter_by(device=device)\
                            .order_by(desc(DeviceInfos.created_at))\
                            .limit(20)\
                            .all()
                for info in all_infos:
                    wind_dict.append({'x': info.created_at.timestamp() * 1000, 'y': info.wind})
                    humidity_dict.append({'x': info.created_at.timestamp() * 1000, 'y': info.humidity})
                    temp_dict.append({'x': info.created_at.timestamp() * 1000, 'y': info.temperature})
                return_list['wind'].append({'name': device.name, 'data': wind_dict})
                return_list['humidity'].append({'name': device.name, 'data': humidity_dict})
                return_list['temp'].append({'name': device.name, 'data': temp_dict})
        return jsonify(return_list)
