import logging

from flask import request, jsonify, abort, Blueprint
from flask_restful import Resource, reqparse
from db.models import Account
from common.db.base import Database
from flask_restful_swagger import swagger


LOG = logging.getLogger(__name__)

PARSER = reqparse.RequestParser()
PARSER.add_argument('user')
PARSER.add_argument('password')
PARSER.add_argument('mail')

class Users(Resource):
    @swagger.operation(
        notes='Route to create new customer',
        parameters=[
            {
              "name": "user",
              "description": "Username for the new account",
              "required": True,
              "allowMultiple": False,
              "dataType": "string"
            },
            {
              "name": "password",
              "description": "Password for the new account",
              "required": True,
              "allowMultiple": False,
              "dataType": "string"
            },
            {
              "name": "mail",
              "description": "Email for the new account",
              "required": True,
              "allowMultiple": False,
              "dataType": "string"
            }
        ],
        responseMessages=[
            {
              "code": 200,
              "message": "Everything went right"
            },
            {
              "code": 415,
              "message": "Missing parameters"
            },
            {
              "code": 405,
              "message": "No username or password given"
            }
        ]
    )
    def post(self):
        params = PARSER.parse_args()
        if not params:
            abort(415)

        username = params.get('user', False)
        password = params.get('password', False)
        email = params.get('mail', False)

        if not username or not password:
            abort(405)

        with Database(auto_commit=True) as db:
            account = db.query(Account).filter_by(name=username).first()
            if not account:
                account = Account(name=username)
                db.add(account)
            if account.password:
                abort(409)

            account.set_password(password)
            account.email = email

            return jsonify(username, account.password, account.email)