import logging

from flask import Flask
from server.extensions import cors, rest_api
from server import views, commands
from config.settings import SETTINGS
from flask.json import JSONEncoder
from datetime import date
from common.server.flaskutils import init_logging


LOG = logging.getLogger(__name__)

"""
How to use ?

1/ import
import logging

2/ create instance
LOG = logging.getLogger(__name__)

3/ use it !
LOG.critical('1')
LOG.error("2")
LOG.warning("3")
LOG.info('4ddd')
LOG.debug('5')
"""

class CustomJSONEncoder(JSONEncoder):

    def default(self, obj):
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


def create_app(config_object=None):
    app = Flask(__name__.split('.')[0])
    app.json_encoder = CustomJSONEncoder
    init_logging()

    app.config.from_object(config_object)
    app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql+psycopg2://" + SETTINGS['POSTGRES_USER'] + ":" + \
        SETTINGS['POSTGRES_PASSWORD'] + '@' + SETTINGS['POSTGRES_HOST'] + ':' + SETTINGS['POSTGRES_PORT'] + '/' + SETTINGS['POSTGRES_DB']

    register_extensions(app)
    register_blueprints(app)
    register_commands(app)

    return app


def register_extensions(app):
    cors.init_app(app)
    rest_api.init_app(app)
    # db.init_app(app)

    return None


def register_blueprints(app):
    """Register Flask blueprints."""
    app.register_blueprint(views.blueprint)

    return None


def register_commands(app):
    """Register Click commands."""
    app.cli.add_command(commands.create_all)
    app.cli.add_command(commands.drop_all)
