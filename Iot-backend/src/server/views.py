import logging
import datetime
import requests

from flask import request, jsonify, abort, Blueprint
from common.server.flaskutils import print_flush
from common.db.tools import serialize_foreign_recursive, to_dict
from common.db.base import Database
from common.utils.security import authentication_required, generate_user_token, permissions
from db.models import Account, DeviceInfos, Device
from sqlalchemy import desc, func
from server import *
from server.extensions import rest_api
from config.settings import SETTINGS
from utils.permissions import Permissions

from server.api.users import Users
from server.api.device_position import DevicePosition
from server.api.login import Login
from server.api.stats import Stats
from server.api.general_stats import GeneralStats
from server.api.health_check import HealthCheck

blueprint = Blueprint('views', __name__)

LOG = logging.getLogger(__name__)

rest_api.add_resource(Users, '/api/users', methods=['POST'])
rest_api.add_resource(DevicePosition, '/api/devices/position', methods=['GET'])
rest_api.add_resource(Login, '/api/users/login', methods=['POST'])
rest_api.add_resource(Stats, '/api/get/stats/<device_given>', methods=['GET'])
rest_api.add_resource(GeneralStats, '/api/get/general_stats/<device_given>', methods=['GET'])
rest_api.add_resource(HealthCheck, '/api/devices/is_up/<device_given>', methods=['GET'])

@blueprint.route('/status/is_up', methods=['GET'])
def is_up():
    return jsonify(is_up=True)
