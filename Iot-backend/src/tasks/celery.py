#!/usr/bin/env python
# coding: utf8
# pylint: disable=ungrouped-imports
from __future__ import absolute_import


import sys
import datetime
import logging
import requests

from server.extensions import celery
# from server.extensions import db
from server.app import create_app
from db.models import Device, DeviceInfos
from common.db.base import Database
from config.settings import SETTINGS

LOG = logging.getLogger(__name__)

app = celery  # pylint: disable=invalid-name
app.init_app(create_app())


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(
        (60),
        get_sensors_infos.s(),
    )


@app.task
def get_sensors_infos():
    # andon_scheduler()
    wind = requests.get(SETTINGS['API_URL'] + SETTINGS['WIND_API']).json()['wind']
    humidity = requests.get(SETTINGS['API_URL'] + SETTINGS['HUMIDITY_API']).json()['humidity']
    temperature = requests.get(SETTINGS['API_URL'] + SETTINGS['TEMP_API']).json()['temperature']

    for (device, device2, device3) in zip(wind, humidity, temperature):
        with Database(auto_commit=True) as db:
            device_name = device['id']
            device_selected = db.query(Device).filter_by(name=device_name).first()

            if not device_selected:
                positions = device['zone'].split(',')
                posx = float(positions[0])
                posy = float(positions[1])
                device_selected = Device(device_name, posx, posy)
                db.add(device_selected)
                db.commit()

            wind_value = device['km/h']
            humidity_value = device2['%']
            temperature_value = device3['°C']

            device_infos = DeviceInfos(wind_value, humidity_value, temperature_value, device_selected)
            db.add(device_infos)
            db.commit()



if __name__ == '__main__':
    print('=========== STARTED CELERY IN MAIN MODE ===========')
    app.start()
