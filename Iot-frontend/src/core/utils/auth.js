import {http} from '../core/network/http.js';
import {user} from './core/utils/User.js'

class Auth {

    function checkValidity() {
        if (!isAuthed()) {
            /*$interval.cancel(timer_refresh);
            $window.location.href = '#/unauth';
            $window.location.reload();*/
            window.location.reload();
        }
        else
        {
            setTimeout(function () {
              this.checkValidity();
            }.bind(this), 5000);
        }
    }

    function isAuthed() {
        var parsedToken = parseJwt(getToken());
        return (parsedToken && isTokenFresh(parsedToken));
    }
    function payload(token)
    {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace('-', '+').replace('_', '/');

        return JSON.parse(window.atob(base64));
    }
    function parseJwt(token) {
        if (!token)
            return false;
        try {
            return payload(token);
        } catch (e) {
            console.error("Invalid Token Session", "JWT error " + e.message);
            service.logout();
            return false;
        }
    }

    function saveToken(token) {
        if (token) {
            window.localStorage.setItem('jwtToken', JSON.stringify(token);
        }
    }

    function getToken() {
        var data = window.localStorage['jwtToken'];
        if (data && data != {} )
        {
            return JSON.parse(data);
        }
        return false;
    }

    function isTokenFresh(parsedToken) {
        if (parsedToken) {
            var is_auth = (Math.round(new Date().getTime() / 1000) <= parsedToken.exp);
            if (is_auth) {
                setTimeout(function () {
                  this.checkValidity();
                }.bind(this), 5000);
            }
            return is_auth;
        } else {
            return false;
        }
    }

    function logout() {
        $window.localStorage.removeItem('jwtToken');
    }

    function authentication_handling() {
        var token = getToken();
        var parsedToken = parseJwt(token);
        if (!parsedToken || !isTokenFresh(parsedToken)) {

            return false;
        }
        // User authed with not expired token, all good. Check services loading
        user.store(parsedToken);
        return true;

    }
}


export let user = new User();