import Vue from 'vue'

export let bus = {
  sessionToCompare: { 'sessionToCompare1': {}, 'sessionToCompare2': {} },
  selectedSession: {},
  warn: { 'modify': {}, 'name': {} },
  shared: { 'qrHide': {} },
  vue: new Vue({})
}
