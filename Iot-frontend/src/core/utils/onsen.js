/* eslint-disable */

export const OnsenUtils = {
  bringPageTop: function (pageStack, pageId) {
    var index = _.findIndex(pageStack, function (o) { return o === pageId })
    if (index >= 0) {
      var currentLast = _.last(pageStack)
      pageStack[pageStack.length - 1] = pageStack[index]
      pageStack[index] = currentLast
    } else {
      pageStack.push(pageId)
    }
  }
}
