import Vue from 'vue'
import App from './App.vue'
import VueOnsen from 'vue-onsenui' // This imports 'onsenui', so no need to import it separately
import Toasted from 'vue-toasted'

import 'onsenui/css/onsenui.css'
import 'onsenui/css/onsen-css-components.css'
import i18n from 'roddeh-i18n'
import french from './assets/french.json'
import VueApexCharts from 'vue-apexcharts'
import * as VueGoogleMaps from 'vue2-google-maps'

import { bus } from './core/utils/bus.js'
import { OnsenUtils } from './core/utils/onsen.js'

require('lodash')
Vue.use(Toasted)
Vue.component('apexchart', VueApexCharts)
/* eslint-disable no-new */

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyC6t_C0MB-g-cg8zPyCM2x9l1aTlM8iWMw',
    libraries: 'places' // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)

    // If you want to set the version, you can do so:
    // v: '3.26',
  }
})

i18n.translator.add(french)
bus.vue.$on('french', () => {
  i18n.translator.add(french)
})

Vue.mixin({
  methods: {
    tr: function (key) {
      return i18n(key)
    },
    goTo: function (page) {
      OnsenUtils.bringPageTop(this.pageStack, page)
    },
    setProtectedTimeout (callback, timeoutInSec) {
      if (this._isDestroyed) {
        return null
      } else {
        return setTimeout(callback, timeoutInSec)
      }
    }
  }
})

Vue.use(VueOnsen) // VueOnsen set here as plugin to VUE. Done automatically if a call to window.Vue exists in the startup code.
Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
