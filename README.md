# Iot project to display devices infos on mobile app
___
### Frontend
`Folder`: Iot-frontend
Made using VueJS + webpack and cordova.
###### Useful commands:
```bash
npm install # Install all node_modules packages
npm run dev # Launch dev server with hot reload

# Building for production:
cordova platform rm <ios/android>
npm run build
cordova platform add <ios/android>
```
###### Description
* Login
* Account creation
* Home page (All displayed infos are refreshed every minute)
    * General Stats for all devices (min/max/average for wind, humidity and temperature)
    * Last 20 values (20 minutes) for humidity, wind and temperature, by default all devices are selected, click on device name to disable it. Possibility to zoom on every graph.
* Settings (All displayed infos are refreshed every minute)
    * Display what device are up, and last health check date
    * Display all device location on Google Map

###### Screenshots
<img src="md-assets/login.png" alt="drawing" width="40%"/>
<img src="md-assets/creation.png" alt="drawing" width="40%"/>
<img src="md-assets/home-1.png" alt="drawing" width="40%"/>
<img src="md-assets/home-2.png" alt="drawing" width="40%"/>
<img src="md-assets/home-3.png" alt="drawing" width="40%"/>
<img src="md-assets/settings.png" alt="drawing" width="40%"/>

___
### Backend
`Folder`: Iot-backend
Made using Docker + Python Flask + SqlAlchemy
###### How to launch
```bash
docker-compose up
docker-compose exec app bash
    > export FLASK_APP=server_app.py
    > flask create-all
```

After launching the docker, you can access a swagger API at:
`<URL>:9019/api/spec.html`

![alt text](md-assets/swagger.png "Title")
Another route is: `/status/is_up`. It says True if the backend is actually up

###### Routes detail
![alt text](md-assets/swagger-detail.png "Title")